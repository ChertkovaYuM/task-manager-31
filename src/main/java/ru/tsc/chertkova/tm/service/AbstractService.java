package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IRepository;
import ru.tsc.chertkova.tm.api.service.IService;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.exception.AbstractException;
import ru.tsc.chertkova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @Nullable
    protected final R repository;

    public AbstractService(@Nullable final R repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public M add(@Nullable final M model) throws AbstractException {
        Optional.ofNullable(model).orElseThrow(() -> new ModelNotFoundException());
        return repository.add(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        return repository.set(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) throws AbstractException {
        Optional.ofNullable(model).orElseThrow(() -> new ModelNotFoundException());
        repository.remove(model);
        return model;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Nullable
    @Override
    public M findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(() -> new IdEmptyException());
        return repository.findById(id);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(() -> new IdEmptyException());
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

}
