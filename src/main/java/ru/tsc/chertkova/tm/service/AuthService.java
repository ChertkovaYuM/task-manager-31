package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.service.IAuthService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.PasswordEmptyException;
import ru.tsc.chertkova.tm.exception.user.AccessDeniedException;
import ru.tsc.chertkova.tm.exception.user.PermissionException;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;

public class AuthService implements IAuthService {

    @Nullable
    private final IPropertyService propertyService;

    @Nullable
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@Nullable final IPropertyService propertyService,
                       @Nullable final IUserService userService) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    public void login(@Nullable final String login,
                      @Nullable final String password) {
        Optional.ofNullable(login).orElseThrow(() -> new LoginEmptyException());
        Optional.ofNullable(password).orElseThrow(() -> new PasswordEmptyException());
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (user.getLocked()) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.salt(propertyService, password))) throw new AccessDeniedException();
        userId = user.getId();
    }

    @NotNull
    @Override
    public User getUser() {
        @Nullable final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public void logout() {
        //if (isAuth()) throw new AccessDeniedException();
        userId = null;
    }

    @Override
    @Nullable
    public User registry(@Nullable final String login,
                         @Nullable final String password,
                         @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @Nullable
    @Override
    public String getUserId() {
        Optional.ofNullable(userId).orElseThrow(() -> new AccessDeniedException());
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        Optional.ofNullable(role).orElseThrow(() -> new PermissionException());
        @NotNull final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
