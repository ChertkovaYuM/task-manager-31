package ru.tsc.chertkova.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.endpoint.Operation;
import ru.tsc.chertkova.tm.dto.request.AbstractRequest;
import ru.tsc.chertkova.tm.dto.response.AbstractResponse;
import ru.tsc.chertkova.tm.task.AbstractServerTask;
import ru.tsc.chertkova.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Server {

    @Getter
    @Nullable
    private ServerSocket serverSocket;

    @NotNull
    private final Bootstrap bootstrap;

    @Getter
    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull final AbstractServerTask task) {
        executorService.submit(task);
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            final Class<RQ> reqClass, Operation<RQ, RS> operation) {
        dispatcher.registry(reqClass, operation);
    }

    public Object call(AbstractRequest request) {
        return dispatcher.call(request);
    }

    @SneakyThrows
    public void start() {
        @NotNull final Integer port = bootstrap
                .getPropertyService().getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        serverSocket.close();
        executorService.shutdown();
    }

}
