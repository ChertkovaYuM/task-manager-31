package ru.tsc.chertkova.tm.command.domain;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.command.AbstractCommand;
import ru.tsc.chertkova.tm.dto.Domain;

@NoArgsConstructor
public abstract class AbstractDomainCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_JAXB_XML = "./data_jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "./data_jaxb.json";

    @NotNull
    public static final String FILE_FASTERXML_YAML = "./data_fasterxml.yaml";

    @NotNull
    public static final String FILE_FASTERXML_XML = "./data_fasterxml.xml";

    @NotNull
    public static final String FILE_FASTERXML_JSON = "./data_fasterxml.json";

    @NotNull
    public static final String JAVAX_XML_BIND_CONTEXT_FACTORY =
            "javax.xml.bind.context.factory";

    @NotNull
    public static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY =
            "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String APPLICATION_JSON = "application/json";

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

}
