package ru.tsc.chertkova.tm.command.system;

public final class VersionCommand extends AbstractSystemCommand {

    public static final String NAME = "version";

    public static final String DESCRIPTION = "Display program version.";

    public static final String ARGUMENT = "-v";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(getServiceLocator().getPropertyService().getApplicationVersion());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

}
