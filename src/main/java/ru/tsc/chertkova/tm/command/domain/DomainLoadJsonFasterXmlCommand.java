package ru.tsc.chertkova.tm.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.Domain;
import ru.tsc.chertkova.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DomainLoadJsonFasterXmlCommand extends AbstractDomainCommand {

    @NotNull
    public static final String NAME = "data-load-json-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load date from json file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
