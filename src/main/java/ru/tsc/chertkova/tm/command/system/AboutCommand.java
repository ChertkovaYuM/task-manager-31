package ru.tsc.chertkova.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class AboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Display developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: " + getServiceLocator().getPropertyService().getAuthorName());
        System.out.println("E-mail: " + getServiceLocator().getPropertyService().getAuthorEmail());
    }

}
