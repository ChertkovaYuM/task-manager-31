package ru.tsc.chertkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.dto.request.ServerAboutRequest;
import ru.tsc.chertkova.tm.dto.request.ServerVersionRequest;
import ru.tsc.chertkova.tm.dto.response.ServerAboutResponse;
import ru.tsc.chertkova.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
