package ru.tsc.chertkova.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
