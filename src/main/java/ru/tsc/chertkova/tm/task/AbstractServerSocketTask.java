package ru.tsc.chertkova.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    public AbstractServerSocketTask(
            @NotNull final Server server,
            @NotNull final Socket socket) {
        super(server);
        this.socket = socket;
    }

}
