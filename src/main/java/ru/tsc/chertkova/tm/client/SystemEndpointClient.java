package ru.tsc.chertkova.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.chertkova.tm.dto.request.ServerAboutRequest;
import ru.tsc.chertkova.tm.dto.request.ServerVersionRequest;
import ru.tsc.chertkova.tm.dto.response.ServerAboutResponse;
import ru.tsc.chertkova.tm.dto.response.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        System.out.println(client.getAbout(new ServerAboutRequest()).getEmail());
        System.out.println(client.getAbout(new ServerAboutRequest()).getName());
        System.out.println(client.getVersion(new ServerVersionRequest()).getVersion());
        client.disconnect();
    }

}
