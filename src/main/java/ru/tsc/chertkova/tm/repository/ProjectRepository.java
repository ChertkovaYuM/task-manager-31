package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

}
